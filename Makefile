TARGET = destest
DES_SRCS = des.c des.key.c boxes.c bitop.c
SRCS = $(DES_SRCS) main.c
CFLAGS = -Wno-all

all:
	@gcc -o $(TARGET) $(CFLAGS) $(SRCS)
	@cat des.c | ./destest encrypt | ./destest decrypt > desc2.c

clean:
	@rm -rf $(TARGET)
