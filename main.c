#include <stdio.h>

#include <string.h>

#include "debug.h"
#include "bitop.h"
#include "types.h"
#include "des.h"

#define BUFFER 4096

bool enc = true;

char keystr[BUFFER] = "GYEONGMINKAIST";
char value[BUFFER];
char data[BUFFER];

int main (int argc, char *argv[]) {
  int len = 0,
      item;
  char *value_ptr = value;
  uint64_t key = *((uint64_t *)keystr);

  memset (value, 0x0, BUFFER);
  memset (data, 0x0, BUFFER);

  if (argc < 2) {
    msg ("Not allowed!%c", '\0');
    return 0;
  }

  if (!strcmp(argv[1], "encrypt"))
    enc = true;
  else
    enc = false;

  while ((item = fgetc (stdin)) != EOF) {
    *value_ptr++ = item;
    len++;
  }

  *value_ptr = '\0';

  if (enc)
    des_encrypt (value, key, data, len);
  else
    des_decrypt (value, key, data, len);

  value_ptr = data;

  int i;
  for (i = 0; i < ((len + 7) / 8) * 8; i++) {
    putchar(*value_ptr++);
  }

  return 0;
}
