#ifndef __DEBUG_H__
#define __DEBUG_H__

#define msg(FORMAT, ...) fprintf(stderr, FORMAT, __VA_ARGS__)

#endif
