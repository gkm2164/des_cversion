#include "boxes.h"
#include "bitop.h"
#include "des.h"

#define TOTAL_KEY_LENGTH 64
#define KEY_LENGTH 56

void
generate_key (uint64_t key, uint64_t keys[16], shift_func shift) {
  int i;
  uint64_t using = permute(key, pc1, KEY_LENGTH, TOTAL_KEY_LENGTH);
  int half = KEY_LENGTH >> 1;

  for (i= 0; i < 16; i++) {
    uint64_t l = bit_trunk (using >> half, uint64_t, half),
             r = bit_trunk (using, uint64_t, half);

    l = shift(l, half, i);
    r = shift(r, half, i);

    using = (l << half) | r;

    keys[i] = permute(using, pc2, half, KEY_LENGTH);
  }
}

uint64_t
make_key (char *key) {
  return *((uint64_t *)key);
}
