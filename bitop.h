#ifndef __BITOP_H__
#define __BITOP_H__

#include "types.h"

#define gen_mask(__dtype__, __bitcnt__) (((__dtype__)1 << (__bitcnt__)) - 1)
#define bit_trunk(__data__, __dtype__, __bitcnt__) (((__data__)) & gen_mask(__dtype__, (__bitcnt__)))

uint64_t permute (uint64_t, int[], int, int);

uint64_t lshift(uint64_t, int, int);
uint64_t rshift(uint64_t, int, int);

void swap(void *a, void *b, uint32_t blocksize);

uint64_t chlr(uint64_t);
uint32_t get_left(uint64_t);
uint32_t get_right(uint64_t);

void showbits(uint64_t, int, int);
void padding (char *, int, int);

#endif
