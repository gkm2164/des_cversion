#include <stdio.h>

#include "debug.h"
#include "types.h"
#include "des.h"
#include "bitop.h"
#include "boxes.h"

uint32_t sbox_func (int n, int boxnum) {
  uint32_t b1 = (n >> 5) & 0x1,
           b2 = n & 0x1;
  uint32_t row = bit_trunk ((b1 << 1) | b2, uint32_t, 2);
  uint32_t col = bit_trunk (n >> 1, uint32_t, 4);

  return sbox[boxnum][row][col];
}

uint64_t expand (uint32_t val) {
  return permute (val, expansion, 48, 32);
}

uint32_t feistel(uint32_t num, uint64_t key) {
  
  uint64_t r = expand (num),
           k = key;

  uint64_t val = r ^ k;
  uint64_t ret = 0;
  int i;

  for (i = 0; i < 8; i++) {
    int shift_cnt = 42 - 6 * i;
    int boxval = bit_trunk (val >> shift_cnt, uint64_t, 6);

    ret = (ret << 4) | sbox_func(boxval, i);
  }

  ret = permute (ret, p, 32, 32);

  return (uint32_t)bit_trunk (ret, uint64_t, 32);
}

uint64_t data_enc (uint64_t val, uint64_t key) {
  uint32_t left = get_left (val),
           right = get_right (val);

  uint64_to_uint32 ret = {
      .bit32.left = right,
      .bit32.right = left ^ feistel (right, key)
    };

  return ret.bit64;
}

uint64_t
run (uint64_t data, uint64_t key, des_dir direction) {
  int i;
  uint64_t keys[16];

  if (direction == encrypt) {
    generate_key (key, keys, lshift);
  } else {
    generate_key (key, keys, rshift);
  }

  data = permute(data, ip, 64, 64);

  for (i = 0; i < 16; i++) {
    data = data_enc(data, keys[i]);
  }

  data = permute(chlr(data), inv_ip, 64, 64);

  return data;
}

static void
_des (uint64_t *data, uint64_t key, uint64_t *output, int length, des_dir dir) {
  int i;
  for (i = 0; i < length; i++) {
    output[i] = run (data[i], key, dir);
  }
}

void
des (void *_src, uint64_t key, bool pad, void *_dst, int len, des_dir dir) {

  uint64_t *src = (uint64_t *)_src;
  uint64_t *dst = (uint64_t *)_dst;

  if (pad) {
    padding (_src, len, sizeof(uint64_t));
  }

  _des(src, key, dst, (len + (sizeof (uint64_t) - 1)) / 8, dir);
}

void
des_encrypt (void *_src, uint64_t key, void *_dst, int len) {
  des (_src, key, true, _dst, len, encrypt);
}

void
des_decrypt (void *_src, uint64_t key, void *_dst, int len) {
  des (_src, key, false, _dst, len, decrypt);
}
