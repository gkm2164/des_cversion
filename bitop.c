#include <stdio.h>
#include <string.h>

#include "debug.h"
#include "types.h"
#include "bitop.h"
#include "boxes.h"

/**/
uint64_t
permute (uint64_t a, int permbox[],
         int arraylen, int bitlen) {
  uint64_t ret = 0;

  int i;

  for (i = 0; i < arraylen; i++) {
    ret = (ret << 1) | bit_trunk (a >> (bitlen - permbox[i]), uint64_t, 1);
  }

  return ret;
}

/**/
uint64_t
lshift (uint64_t val, int bitcnt, int idx) {

  int shiftcnt = lshift_cnt[idx];

  uint64_t left = bit_trunk (val >> (bitcnt - shiftcnt), uint64_t, shiftcnt),

           right = bit_trunk (val, uint64_t, bitcnt - shiftcnt);

  uint64_t ret = (right << shiftcnt) | left;

  return ret;
}

/**/
uint64_t
rshift (uint64_t val, int bitcnt, int idx) {

  int shiftcnt = rshift_cnt[idx];

  uint64_t left = bit_trunk (val >> shiftcnt, uint64_t, bitcnt - shiftcnt),

           right = bit_trunk (val, uint64_t, shiftcnt);

  uint64_t ret = (right << (bitcnt - shiftcnt)) | left;
  
  return ret;
}

/**/
void
swap (void *_a, void *_b, uint32_t blocksize) { 
  char *a = (char *)_a;
  char *b = (char *)_b;

  while(blocksize--) {
    int temp = *a;
    *a = *b;
    *b = temp;
    a++; b++;
  }

}

/**/
uint64_t
chlr (uint64_t data) {
  uint64_to_uint32 ret;
  
  ret.bit64 = data;
  
  swap(&ret.bit32.left, &ret.bit32.right, 4);

  return ret.bit64;
}

/**/
uint32_t
get_left (uint64_t data) {
  uint64_to_uint32 ret;

  ret.bit64 = data;
  
  return ret.bit32.left;
}

/**/
uint32_t
get_right (uint64_t data) {
  uint64_to_uint32 ret;
  
  ret.bit64 = data;
  
  return ret.bit32.right;
}

/**/
void
showbits(uint64_t bits, int chunksize, int cnt) {
  int length = chunksize * cnt;
  int i;

  for (i = 0; i < length; i++) {
    int shiftcnt = length - i - 1;

    if (!(i % chunksize))
      msg("%c", ' ');
    msg("%ld", (long)((bits >> shiftcnt) & 0x1));
  }

  msg("%c", '\n');
}

/**/
void
padding (char *ptr, int len, int chunksize) {
  int end = (len + (chunksize - 1)) / chunksize;  // Celing +1 chunksize

  char *p = ptr + len,
       *pend = ptr + end * chunksize;

  while (p != pend) *p++ = '\0';  
}
