#ifndef __DES_H__
#define __DES_H__

#include "types.h"
#include "des_types.h"

/*
  DES function
*/

void des_encrypt(void *, uint64_t, void *, int);
void des_decrypt(void *, uint64_t, void *, int);
void generate_key(uint64_t, uint64_t[16], shift_func);

#endif
