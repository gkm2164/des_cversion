#ifndef __TYPES_H__
#define __TYPES_H__

#include <stdint.h>

typedef enum { false = 0, true } bool;

typedef union {

  struct {
    uint32_t right;
    uint32_t left;
  } bit32;

  uint64_t bit64;
} uint64_to_uint32;

typedef uint64_t (*shift_func) (uint64_t, int bitcnt, int shiftcnt);

#endif
