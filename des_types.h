#ifndef __DES_TYPES_H__
#define __DES_TYPES_H__

typedef unsigned long long block64_t;

typedef block64_t (*feistel_func)(int num, int k);

typedef enum { encrypt = 0, decrypt } des_dir;

#endif
