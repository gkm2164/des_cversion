#ifndef __BOXES_H__
#define __BOXES_H__

#include "types.h"

extern int ip[64];
extern int inv_ip[64];
extern int sbox[8][4][16];
extern int expansion[48];
extern int p[32];

extern int pc1[56];
extern int pc2[48];
extern int lshift_cnt[16];
extern int rshift_cnt[16];


#endif
